﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Microsoft.Win32;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.Networking;

public class UI_Manager : MonoBehaviour
{
    [SerializeField]
    private static List<Tilesheet> m_Tilesheets = new List<Tilesheet>();
    [SerializeField]
    private InputField m_TextInput;
    [SerializeField]
    private Image m_DisplayImage;
    public void OpenFileDialogOnClick()
    {
        string path = "C:\\Users\\Lukas Waldöfner\\Downloads\\template.png"; // m_TextInput.text;//= EditorUtility.OpenFilePanel("Open Explorer", "", ".png, .jpeg, .jpg");
        if (!File.Exists(path) || !(path.Substring(path.Length - 4) == ".png" || path.Substring(path.Length - 4) == ".jpg"))
        {
            return;
        }

        //BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open));
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        byte[] fileContent = File.ReadAllBytes(path);
        Texture2D LoadedImage = new Texture2D(2, 2);
        LoadedImage.LoadImage(fileContent);
        m_Tilesheets.Add(new Tilesheet(LoadedImage, 64, 64));

        m_DisplayImage.sprite = m_Tilesheets[0].m_Tiles[0].sprite;

    }

    public class Tilesheet
    {
        public readonly Texture2D m_BasicImage;
        public List<Tile> m_Tiles = new List<Tile>();
        private uint m_TileHeight = 1;
        private uint m_TileWidth = 1;

        public Tilesheet(Texture2D _basicImage)
        {
            m_BasicImage = _basicImage;
        }

        public Tilesheet(Texture2D _basicImage, uint _tileSize)
        {
            m_BasicImage = _basicImage;
            m_TileHeight = m_TileWidth = _tileSize;
            Slice();
        }

        public Tilesheet(Texture2D _basicImage, uint _tileHeight, uint _tileWidth)
        {
            m_BasicImage = _basicImage;
            m_TileHeight = _tileHeight;
            m_TileWidth = _tileWidth;
            Slice();
        }

        public void Slice()
        {
            if (m_BasicImage == null || m_TileHeight <= 0 || m_TileWidth <= 0)
            {
                //TODO: Coroutine telling the error
                return;
            }

            for (int i = 0; i < m_BasicImage.height; i += (int)m_TileHeight)
            {
                for (int j = 0; j < m_BasicImage.width; j+= (int)m_TileWidth)
                {
                    Tile tile = new Tile();
                    Debug.Log(j + ", " + i + ", " + m_TileWidth + ", " + m_TileHeight);
                    tile.sprite = Sprite.Create(m_BasicImage, new Rect(j, i , m_TileWidth, m_TileHeight), new Vector2(m_TileWidth / 2, m_TileHeight / 2));
                    m_Tiles.Add(tile);
                }
            }
        }
    }
}